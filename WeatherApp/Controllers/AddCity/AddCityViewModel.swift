//
//  AddCityViewModel.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

final class AddCityViewModel {
    
    typealias Dependencies = HasCitiesServiceProtocol
    private let dependencies: Dependencies

    var updateBackgroundView: ((CustomViewHandler.CustomViewType) -> ())?
    var reloadList: (() -> ())?
    var dataList = [City]()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    func getFavoriteCities(query: String) {
        let list = self.dependencies.getCitiesService.getCitiesList(query: query)
        self.dataList = list
        self.reloadList?()
    }
}
