//
//  AddCityViewController.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import UIKit

protocol AddCityViewControllerProtocol: AnyObject {
    func didSelectCityToAdd(city: City)
}

final class AddCityViewController: UIViewController {

    static func instantiate() -> AddCityViewController {
        struct DI: AddCityViewModel.Dependencies {
            var getCitiesService: CitiesRepositoriesProtocol = CitiesRepositoriesImpl()
        }
        return AddCityViewController(dependencies: DI())
    }

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    private let viewModel: AddCityViewModel!

    weak var delegate: AddCityViewControllerProtocol?

    private init(dependencies: AddCityViewModel.Dependencies) {
        self.viewModel = AddCityViewModel(dependencies: dependencies)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Add City"
        self.configureTableView()
        self.bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.getFavoriteCities(query: "")
    }

    private func configureTableView() {
        self.tableView.rowHeight = 60
    }
    
    func bindViewModel() {
        self.viewModel.updateBackgroundView = { [weak self] type in
            guard let self = self else { return }

            if let errorView = type.getCustomView() {
                self.tableView.backgroundView = ErrorView(type: errorView)
            } else {
                self.tableView.backgroundView = nil
            }
        }
        
        self.viewModel.reloadList = { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }
}
extension AddCityViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.getFavoriteCities(query: searchText)
    }
}
extension AddCityViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let city = self.viewModel.dataList[indexPath.row]
        cell.textLabel?.text = (city.name ?? "") + ", " + (city.country ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.delegate?.didSelectCityToAdd(city: self.viewModel.dataList[indexPath.row])
        
        self.navigationController?.popViewController(animated: true)
    }
}
