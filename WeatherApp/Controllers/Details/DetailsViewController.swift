//
//  DetailsViewController.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import UIKit

final class DetailsViewController: UIViewController {

    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var hum: UILabel!
    
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    let viewModel: DetailsViewModel!

    init(dependencies: DetailsViewModel.Dependencies, myCity: FavoriteCityResponseModel) {
        self.viewModel = DetailsViewModel(dependencies: dependencies, city: myCity)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.setupDetailsView()
    }

    func bindViewModel() {
        
        self.viewModel.loadingView = { [weak self] isLoading in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if isLoading {
                    LoadingOverlay.shared.showOverlay(view: self.view)
                } else {
                    LoadingOverlay.shared.hideOverlayView()
                }
            }
        }

        self.viewModel.reloadView = { [weak self] city in
            guard let self = self else { return }
            self.temp.text = city.temperature
            self.name.text = city.name + "," + city.country
            self.pressure.text = "\(city.meteoObject?.current?.pressure ?? 99999)"
            self.hum.text = "\(city.meteoObject?.current?.humidity ?? 9999)"
            self.windSpeed.text = "\(city.meteoObject?.current?.windSpeed ?? 99999)"
            self.weatherDescription.text = "\(city.meteoObject?.current?.weather?.first?.main ?? "--"), \(city.meteoObject?.current?.weather?.first?.weatherDescription ?? "--")"
            if let imageName = city.meteoObject?.current?.weather?.first?.icon, let image = UIImage(named: imageName) {
                self.weatherImage.image = image
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
