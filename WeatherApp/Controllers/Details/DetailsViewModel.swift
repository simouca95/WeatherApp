//
//  DetailsViewModel.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

final class DetailsViewModel {

    typealias Dependencies = HasWeatherRepositoryProtocol
    let dependencies: Dependencies
    var reloadView: ((FavoriteCityResponseModel) -> ())?

    var loadingView: ((Bool) -> ())?

    var city: FavoriteCityResponseModel

    init(dependencies: Dependencies, city: FavoriteCityResponseModel) {
        self.dependencies = dependencies
        self.city = city
    }

    func setupDetailsView() {
        self.loadingView?(true)
        self.dependencies.weatherService.fetchCityWeatherData(lat: self.city.latitude,
                                                              lon: self.city.longitude) { result in
            switch result {
            case .success(let meteo):
                self.city.meteoObject = meteo
            case .failure(_):
                break
            }
            DispatchQueue.main.async {
                self.loadingView?(false)
                self.reloadView?(self.city)
            }
        }
    }
}
