//
//  HomeViewController.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import UIKit

final class HomeViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    static func instantiate() -> HomeViewController {

        struct DI: HomeViewModel.Dependencies {
            var weatherService: WeatherRepositoryProtocol = WeatherDataProvider()
            var favoriteCitiesService: FavoriteCitiesRepositoryProtocol = FavoriteCitiesProvider()
        }
        return HomeViewController(dependencies: DI())
    }

    private let viewModel: HomeViewModel!
    private let dependencies: HomeViewModel.Dependencies

    private init(dependencies: HomeViewModel.Dependencies) {
        self.dependencies = dependencies
        self.viewModel = HomeViewModel(dependencies: dependencies)
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Weather App"
        self.configureTableView()
        self.setupNavigationBar()
        self.bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.getFavoriteCities()
    }

    private func setupNavigationBar() {
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCityAction))
        self.navigationItem.setRightBarButton(add, animated: false)
    }

    private func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = 60
        self.tableView.register(UINib(nibName: "FavoriteCityCell", bundle: nil), forCellReuseIdentifier: "FavoriteCityCell")

    }

    private func bindViewModel() {
        self.viewModel.updateBackgroundView = { [weak self] type in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                if let errorView = type.getCustomView() {
                    self.tableView.backgroundView = ErrorView(type: errorView)
                } else {
                    self.tableView.backgroundView = nil
                }
            }
        }

        self.viewModel.reloadList = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    @objc private func addCityAction() {
        let vc = AddCityViewController.instantiate()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - TableView delegates
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCityCell", for: indexPath) as! FavoriteCityCell
        cell.cityModel = self.viewModel.dataList[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        if Reachability.isConnectedToNetwork() {
            let detailVC = DetailsViewController(dependencies: self.dependencies, myCity: self.viewModel.dataList[indexPath.row])
            self.navigationController?.pushViewController(detailVC, animated: true)
        } else {
            
        }

    }
}

// MARK: - AddCityViewControllerProtocol delegates
extension HomeViewController: AddCityViewControllerProtocol {
    func didSelectCityToAdd(city: City) {
        self.viewModel.addFavoriteCity(city: city)
    }
}
