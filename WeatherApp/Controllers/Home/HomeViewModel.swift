//
//  HomeViewModel.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

final class HomeViewModel {
    
    typealias Dependencies = HasFavoriteCitiesRepositoryProtocol & HasWeatherRepositoryProtocol
    private let dependencies: Dependencies

    var updateBackgroundView: ((CustomViewHandler.CustomViewType) -> ())?
    var reloadList: (() -> ())?
    var dataList = [FavoriteCityResponseModel]() {
        didSet {
            
        }
    }
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(fetchChanges),
            name: NSNotification.Name(rawValue: "NSPersistentStoreRemoteChangeNotification"),
            object: nil)
    }

    @objc func fetchChanges() {
        // self.getFavoriteCities()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func getFavoriteCities() {
        print("getting FavoriteCities local list ....")

        let result = self.dependencies.favoriteCitiesService.getFavoriteCitiesList()
        switch result {
        case .success(let list):
            print("FavoriteCities local list done")
            if list.isEmpty {
                self.updateBackgroundView?(.emptyList)
            } else {
                self.updateBackgroundView?(.none)
                self.dataList = list
                self.reloadList?()

                print("fetching All FavoriteCities weather data ....")
                if Reachability.isConnectedToNetwork() {
                    self.dependencies.weatherService.fetchAllWeatherData(storedCities: list) { [weak self] updatedList in
                        guard let self = self else { self?.reloadList?(); return }
                        print("fetched All FavoriteCities weather data done")
                        self.dataList = updatedList
                        self.reloadList?()
                        _ = self.dependencies.favoriteCitiesService.updateFavoriteCitiesList(updatedList)
                    }
                }

            }

        case .failure(_):
            self.dataList = []
            self.updateBackgroundView?(.errorView)
        }
    }
    
    func addFavoriteCity(city: City) {
        print("creating city \(city.name ?? "aaaaaaaa") ....")

        var cityToCreate = FavoriteCityRequestModel(name: city.name,
                                                    country: city.country,
                                                    latitude: city.lat!,
                                                    longitude: city.lng!,
                                                    temperature: "--")

        guard let lat = city.lat, let lon = city.lng else {
            let _ = self.dependencies.favoriteCitiesService.createNewFavoriteCity(cityToCreate)
            self.getFavoriteCities()
            return
        }

        self.dependencies.weatherService.fetchCityWeatherData(lat: lat, lon: lon) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let meteoResponse):
                if let temp = meteoResponse.current?.temp {
                    cityToCreate.temperature = "\(temp)"
                }
            case .failure(_):
                break
            }
            let _ = self.dependencies.favoriteCitiesService.createNewFavoriteCity(cityToCreate)
            self.getFavoriteCities()
        }
    }
}
