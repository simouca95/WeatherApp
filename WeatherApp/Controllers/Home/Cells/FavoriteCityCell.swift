//
//  FavoriteCityCell.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import UIKit

class FavoriteCityCell: UITableViewCell {
    
    @IBOutlet private weak var cityName: UILabel!
    @IBOutlet private weak var cityTemp: UILabel!
    @IBOutlet private weak var cityTempAvatar: UIImageView!

    var cityModel: FavoriteCityResponseModel! {
        didSet {
            self.updateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func updateCell() {
        self.cityName.text = cityModel.name
        self.cityTemp.text = "\(cityModel.temperature)"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.cityTemp.text = ""
        self.cityName.text = ""
    }
}
