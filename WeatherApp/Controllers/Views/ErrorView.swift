//
//  ErrorView.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import UIKit

class ErrorView: UIView {
    
    private(set) var backgroundImage: UIImageView!
    private(set) var backgroundText: UILabel!
    
    init(type: CustomViewHandler) {
        super.init(frame: .zero)
        self.setupCustomView(type: type)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCustomView(type: CustomViewHandler) {
        
        // backgroundImage
        let backgroundImage = UIImageView(frame: .zero)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.image = type.image
        backgroundImage.contentMode = .scaleAspectFit
        self.backgroundImage = backgroundImage
        self.addSubview(backgroundImage)
        
        // backgroundText
        let backgroundText = UILabel(frame: .zero)
        backgroundText.translatesAutoresizingMaskIntoConstraints = false
        backgroundText.textAlignment = .center
        backgroundText.text = type.text
        self.backgroundText = backgroundText
        self.addSubview(backgroundText)
        
        NSLayoutConstraint.activate([
            // backgroundImage
            backgroundImage.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            backgroundImage.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            backgroundImage.widthAnchor.constraint(equalToConstant: 200),
            backgroundImage.heightAnchor.constraint(equalToConstant: 200),
            
            // backgroundText
            backgroundText.topAnchor.constraint(equalTo: backgroundImage.bottomAnchor, constant: 20),
            backgroundText.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor, constant: -20),
            backgroundText.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor, constant: 20)
        ])
    }
}

struct CustomViewHandler {
    var image: UIImage
    var text: String
    
    enum CustomViewType {
        case emptyList
        case errorView
        case none
        
        func getCustomView() -> CustomViewHandler? {
            switch self {
            case .emptyList:
                return CustomViewHandler(image: UIImage(named: "emptyList")!, text: "No Cities Selected")
            case .errorView:
                return CustomViewHandler(image: UIImage(named: "error")!, text: "Generic failure")
            case .none:
                return nil
                
            }
        }
    }
}
