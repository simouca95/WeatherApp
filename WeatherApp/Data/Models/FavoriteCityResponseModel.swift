//
//  FavoriteCity.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation
import WeatherFramwork

struct FavoriteCityResponseModel: Identifiable, Equatable {
    static func == (lhs: FavoriteCityResponseModel, rhs: FavoriteCityResponseModel) -> Bool {
        lhs.id == rhs.id
    }

    let id: UUID
    var name: String
    var country: String
    var latitude: String
    var longitude: String
    var temperature: String
    var meteoObject: MeteoResponse?
    
    init(id: UUID, name: String, country: String, latitude: String, longitude: String, temperature: String) {
        self.id = id
        self.name = name
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
        self.temperature = temperature
    }
}

struct FavoriteCityRequestModel: Equatable {

    var name: String?
    var country: String?
    var latitude: String?
    var longitude: String?
    var temperature: String?
    
    init(name: String?, country: String?, latitude: String?, longitude: String?, temperature: String?) {
        self.name = name
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
        self.temperature = temperature
    }
    
}
