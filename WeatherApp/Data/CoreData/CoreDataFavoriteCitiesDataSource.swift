//
//  CoreDataFavoriteCitiesDataSource.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

protocol FavoriteCitiesDataSourceProtocol {
    
    func getAll() -> Result<[FavoriteCityResponseModel], CoreDataError>
    
    func getOne(_ id: UUID) -> Result<FavoriteCityResponseModel?, CoreDataError>
    
    func create(_ city: FavoriteCityRequestModel) -> Result<Bool, CoreDataError>
    
    func update(id: UUID, temperature: String) -> Result<Bool, CoreDataError>

    func update(list: [FavoriteCityResponseModel]) -> Result<Bool, CoreDataError>
}

class CoreDataFavoriteCitiesDataSource : FavoriteCitiesDataSourceProtocol {
    
    
    let dbWrapper: CoreDataWrapperProtocol
    
    init() {
        self.dbWrapper = CoreDataWrapper()
    }
    
    private func mapToFavoriteCityResponse(cityEntity: MyCity) -> FavoriteCityResponseModel {
        
        return FavoriteCityResponseModel(id: cityEntity.id!,
                                         name: cityEntity.name!,
                                         country: cityEntity.country!,
                                         latitude: cityEntity.latitude!,
                                         longitude: cityEntity.longitude!,
                                         temperature: cityEntity.temperature!)
    }
    
    private func _getAll() throws -> [MyCity] {
        let result: [MyCity] = try dbWrapper.getData(entityName: "MyCity", predicate: nil) as! [MyCity]
        return result
    }
    
    private func _getOne(id: UUID) throws -> MyCity {
        let result: [MyCity] = try dbWrapper.getData(entityName: "MyCity", predicate: NSPredicate(format: "id = %@", id.uuidString)) as! [MyCity]
        return result[0]
    }
}

// MARK: - FavoriteCitiesDataSourceProtocol delegate
extension CoreDataFavoriteCitiesDataSource {
    func getOne(_ id: UUID)  -> Result<FavoriteCityResponseModel?, CoreDataError> {
        do{
            let data = try _getOne(id: id)
            return .success(mapToFavoriteCityResponse(cityEntity: data))
        }catch{
            return .failure(.Get)
        }
    }
    
    func getAll() -> Result<[FavoriteCityResponseModel], CoreDataError> {
        do{
            let data = try _getAll()
            
            return .success(data.map({ myCityEntity in
                mapToFavoriteCityResponse(cityEntity: myCityEntity)
            }))
        }catch{
            return .failure(.Get)
        }
    }
    
    func create(_ favoriteCityRequestModel: FavoriteCityRequestModel)  -> Result<Bool, CoreDataError> {
        do{
            
            let newFavoriteCity = MyCity(context: dbWrapper.getContext())
            newFavoriteCity.id = UUID()
            newFavoriteCity.name = favoriteCityRequestModel.name
            newFavoriteCity.country = favoriteCityRequestModel.country
            newFavoriteCity.latitude = favoriteCityRequestModel.latitude
            newFavoriteCity.longitude = favoriteCityRequestModel.longitude
            newFavoriteCity.temperature = favoriteCityRequestModel.temperature
            try dbWrapper.saveContext()
            print("create city \(newFavoriteCity.name ?? "aaaaaaaa") successfully")
            return .success(true)
        }catch{
            return .failure(.Create)
        }
        
    }
    
    func update(id: UUID, temperature: String) -> Result<Bool, CoreDataError> {
        do{
            let oldData = try _getOne(id: id)
            oldData.temperature = temperature
            print("updating city \(oldData.name) successfully")
            return .success(true)
        }catch{
            return .failure(.Update)
        }
    }
    
    func update(list: [FavoriteCityResponseModel]) -> Result<Bool, CoreDataError> {
        
        do{
            for object in list {
                _ = self.update(id: object.id, temperature: object.temperature)
            }
            
            try dbWrapper.saveContext()
            print("updating cities successfully")
            return .success(true)
        }catch{
            return .failure(.Update)
        }
    }
}

