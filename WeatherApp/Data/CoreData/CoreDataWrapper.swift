//
//  CoreDataWrapper.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation
import CoreData
import UIKit

protocol CoreDataWrapperProtocol {

    func getData(entityName: String, predicate: NSPredicate?) throws -> [NSManagedObject]

    func saveContext() throws

    func getContext() -> NSManagedObjectContext
}

class CoreDataWrapper : CoreDataWrapperProtocol {

    private static var container: NSPersistentContainer = {
             let container = NSPersistentContainer(name: "WeatherApp")
             container.loadPersistentStores { description, error in
                 if let error = error {
                      fatalError("Unable to load persistent stores: \(error)")
                 }
             }
             return container
         }()
     
     var context: NSManagedObjectContext {
         return Self.container.viewContext
     }
    
    init() {        
        // turn on remote change notifications
//        let remoteChangeKey = "NSPersistentStoreRemoteChangeNotificationOptionKey"
//        let description = container.persistentStoreDescriptions.first
//        description?.setOption(true as NSNumber, forKey: remoteChangeKey)
    }
    
    func save() throws {
        if context.hasChanges {
            do {
                try context.save()
                print("CoreDatawrapper save is done")
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func getContext() -> NSManagedObjectContext {
        return context
    }

    func getData(entityName: String, predicate: NSPredicate? = nil) throws -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        fetchRequest.predicate = predicate
        let entities = try context.fetch(fetchRequest)
        return entities
    }
    
    func saveContext() throws{
        try save()
    }
}
