//
//  CitiesProvider.swift
//  WeatherFramwork
//
//  Created by sami hazel on 24/09/2022.
//

import Foundation

public final class CitiesProvider {

    /// Full response of `cities` json
    public static let cities: [City] = {
        CitiesProvider.decodeCities().sorted(by: {$0.name?.lowercased() ?? "z" < $1.name?.lowercased() ?? "z"})
    }()


    // MARK: - Countries
    public static func decodeCities() -> [City] {
        
        print("decode cities entered")
        let path = Bundle.main.path(forResource: "cities", ofType: "json")
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
            let cities = try JSONDecoder().decode([City].self, from: data)
            return CitiesProvider.removeStoredCities(cities)
        } catch {
            print("[CitiesProvider] getCountries, unable to decode countries response")
            return []
        }
    }

    public static func removeStoredCities(_ list: [City]) -> [City] {
        let result = FavoriteCitiesProvider().getFavoriteCitiesList()
        switch result {
        case .success(let storedList):
            let cities = list.filter { city in
                !storedList.contains(where: { $0.name == city.name && $0.country == city.country})
            }
            return cities
        case .failure(_):
            return list
        }
    }

    /** Get cities */
    public static func getFiltredCities(str: String) -> [City] {
        if str.isEmpty {
            return CitiesProvider.cities
        }
        return CitiesProvider.cities.filter { $0.name?.contains(str) ?? false }

    }
}
