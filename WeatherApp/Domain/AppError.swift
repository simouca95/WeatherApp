//
//  AppError.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

enum AppError: Error {
    case network
    case invalidURL
}

enum CoreDataError: Error {
    case Create
    case Get
    case Update
    case Delete
}
