//
//  WeatherDataProvider.swift
//  WeatherApp
//
//  Created by sami hazel on 26/09/2022.
//

import Foundation
import WeatherFramwork

struct WeatherDataProvider: WeatherRepositoryProtocol {
    
    let sdk = WeatherSDK(apiKey: Config.API.apiKey)
    let coreDataHandler = CoreDataFavoriteCitiesDataSource()
    
    func fetchAllWeatherData(storedCities: [FavoriteCityResponseModel], _ completion: @escaping ([FavoriteCityResponseModel]) -> ()) {
        print("Fetching All weather data ...")
        let group = DispatchGroup()
        var citiesToUpdate = [FavoriteCityResponseModel]()
        
        for city in storedCities {
            group.enter()
            print("Fetching item city weather data ...\(city.name)")
            sdk.getCurrentWeather(from: city.latitude, lon: city.latitude) { r in
                var cityToUpdate = city
                switch r {
                case .success(let meteo):
                    cityToUpdate.temperature = "\(meteo.current?.temp ?? 999.9)"
                case .failure(_):
                    break
                }
                citiesToUpdate.append(cityToUpdate)
                print("\(cityToUpdate.name) data fetched")
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            completion(citiesToUpdate)
        }
    }
    
    func fetchCityWeatherData(lat: String, lon: String, completion: @escaping (Result<MeteoResponse,Environment.APIError>) -> Void) {
        print("Fetching city weather data \(lat) , \(lon) ...")
        sdk.getCurrentWeather(from: lat, lon: lon) { r in
            switch r {
            case .failure(let error):
                completion(.failure(error))
            case .success(let meteo):
                print("\(lat),\(lon) data fetched")
                completion(.success(meteo))
            }
        }
    }
}
