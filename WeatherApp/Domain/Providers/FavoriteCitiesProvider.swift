//
//  FavoriteCitiesProvider.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

struct FavoriteCitiesProvider: FavoriteCitiesRepositoryProtocol {

    private let favoriteCitiesDataSource: FavoriteCitiesDataSourceProtocol
    
    init(){
        self.favoriteCitiesDataSource = CoreDataFavoriteCitiesDataSource()
    }

    func getFavoriteCitiesList() -> Result<[FavoriteCityResponseModel], CoreDataError> {
        self.favoriteCitiesDataSource.getAll()
    }
    
    func getFavoriteCity(_ id: UUID) -> Result<FavoriteCityResponseModel?, CoreDataError> {
        self.favoriteCitiesDataSource.getOne(id)
    }
    
    func updateFavoriteCity(id: UUID, temperature: String) -> Result<Bool, CoreDataError> {
        self.favoriteCitiesDataSource.update(id: id, temperature: temperature)
    }

    func updateFavoriteCitiesList(_ list: [FavoriteCityResponseModel]) -> Result<Bool, CoreDataError> {
        self.favoriteCitiesDataSource.update(list: list)
    }

    func createNewFavoriteCity(_ city: FavoriteCityRequestModel) -> Result<Bool, CoreDataError> {
        self.favoriteCitiesDataSource.create(city)
    }
    
}
