//
//  CitiesRepositoriesImpl.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

struct CitiesRepositoriesImpl: CitiesRepositoriesProtocol {

    func getCitiesList(query: String) -> [City] {
        CitiesProvider.getFiltredCities(str: query)
    }
}
