//
//  WeatherRepositoryProtocol.swift
//  WeatherApp
//
//  Created by sami hazel on 26/09/2022.
//

import Foundation
import WeatherFramwork

protocol WeatherRepositoryProtocol {
    
    func fetchAllWeatherData(storedCities: [FavoriteCityResponseModel], _ completion: @escaping ([FavoriteCityResponseModel]) -> ())
    func fetchCityWeatherData(lat: String, lon: String, completion: @escaping (Result<MeteoResponse,Environment.APIError>) -> Void)

}

protocol HasWeatherRepositoryProtocol {
    var weatherService: WeatherRepositoryProtocol { get }
}
