//
//  FavoriteCitiesRepositoryProtocol.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation

protocol FavoriteCitiesRepositoryProtocol {
    
    func getFavoriteCitiesList() -> Result<[FavoriteCityResponseModel], CoreDataError>
    
    func getFavoriteCity(_ id: UUID) -> Result<FavoriteCityResponseModel?, CoreDataError>
    
    func updateFavoriteCity(id: UUID, temperature: String) -> Result<Bool, CoreDataError>
    
    func updateFavoriteCitiesList(_ list: [FavoriteCityResponseModel]) -> Result<Bool, CoreDataError>

    func createNewFavoriteCity(_ city: FavoriteCityRequestModel) -> Result<Bool, CoreDataError>
}

protocol HasFavoriteCitiesRepositoryProtocol {
    var favoriteCitiesService: FavoriteCitiesRepositoryProtocol { get }
}
