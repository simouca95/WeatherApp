//
//  CitiesRepositoriesProtocol.swift
//  WeatherApp
//
//  Created by sami hazel on 25/09/2022.
//

import Foundation
import WeatherFramwork
protocol CitiesRepositoriesProtocol {
    
    func getCitiesList(query: String) -> [City]
}

protocol HasCitiesServiceProtocol {
    var getCitiesService: CitiesRepositoriesProtocol { get }
}
